﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{

    void Start()
    {

    }

    //Makes Coin dissapear after being picked up.
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.GetComponent<PlayerMovement>().coins++;
            Destroy (gameObject);
         }
    }

    void Update()
    {

    }
}
