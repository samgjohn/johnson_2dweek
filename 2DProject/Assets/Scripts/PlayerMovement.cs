﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float jumpForce;

    Rigidbody2D rB2D;

    public float runSpeed = 500f;

    public SpriteRenderer spriteRenderer;

    public Animator animator;

    public int coins = 0;

    public int health = 5;

    void Start()
    {
        rB2D = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {

            int levelMask = LayerMask.GetMask("Level");

            if (Physics2D.BoxCast(transform.position, new Vector2(1f, 1f), 0f, Vector2.down, .01f, levelMask))
            {
            Jump();
            }
        }
    }

    void FixedUpdate()
    {
        //Only run left to right
        float horizontalInput = Input.GetAxis("Horizontal");

        rB2D.velocity = new Vector2(horizontalInput * runSpeed * Time.deltaTime, rB2D.velocity.y);

        if (rB2D.velocity.x > 0) {
            spriteRenderer.flipX = false;
        }else
        if (rB2D.velocity.x <0){
            spriteRenderer.flipX = true;
        }

        if (Mathf.Abs(horizontalInput) > 0f) {
            animator.SetBool("IsRunning", true);
        }else
        {
            animator.SetBool("IsRunning", false);
        }
    }

//Displays coin count
    private void OnGUI()
    {
        GUI.Label(new Rect(10,10,100,20),"Coins: " + coins);

        GUI.Label(new Rect(100,10,100,20),"Health: " + health);
    }

    public void Death() {
         if (health == 0) {
             Application.LoadLevel(Application.loadedLevel);
         }
    }

    void Jump(){
        rB2D.velocity = new Vector2(rB2D.velocity.x, jumpForce);
    }
}
